<?php
namespace App\Models;

/**
 * Class Person
 */
class Person {
  protected $name;

  /*
   * Sets the Person name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * Returns the Person name
   * @return mixed
   */
  public function getName()
  {
    return $this->name;
  }
}